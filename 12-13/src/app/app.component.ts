import {Component, OnDestroy, OnInit} from '@angular/core';
import {Person} from './shared/models/person.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Список';
  persons: Person[] = [];

  searching: object = {
    firstName: '',
    lastName: ''
  };

  ngOnInit(): void {
    this.persons.push(new Person('Надежда', 'Колезнева', '+7 (904) 123-45-67', 1));
    this.persons.push(new Person('Пелагея', 'Золотарева', '+7 (900) 000-333-21', 2));
    this.persons.push(new Person('Валерий', 'Хелпмиев', '+7 (922) 777-88-99', 3));
    this.persons.push(new Person('Никола', 'Тесла', '+7 (940) 444-55-66', 4));
    this.persons.push(new Person('Баша', 'Даженова', '+7 (908) 333-22-11', 5));
    this.persons.push(new Person('Григорий', 'Вор', '+7 (800) 555-35-35', 6));
    this.persons.push(new Person('Славянка', 'Истровский', '+7 (900) 555-35-35', 6));
  }

  onAddPerson(person: Person) {
    person.id = (this.persons.length !== 0) ? this.persons[this.persons.length - 1].id + 1 : 1;
    this.persons.push(person);
  }

  onRemovePerson(id: number) {
    this.persons = this.persons.filter(person => person.id !== id);
  }

  onFilterPersonsList(e: object) {
    this.searching = e;
  }
}
