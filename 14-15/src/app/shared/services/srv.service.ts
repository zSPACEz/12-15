import {Injectable} from '@angular/core';
import {Person} from '../models/person.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SrvService {
  persons: Person[] = [];
  link = 'http://api.std-692.ist.mospolytech.ru/';
  options = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  constructor(public http: HttpClient) {
  }

  async getPersons() {
    this.persons = [];
    const data = await this.http
      .get(this.link)
      .toPromise();

    for (const index in data) {
      delete data[index].createdAt;
      delete data[index].updatedAt;
      this.persons.push(data[index]);
    }
  }

  async addPerson(person: Person) {
    delete person.id;
    return this.http.post(this.link, person, this.options).toPromise();
  }

  async removePerson(id: number) {
    return this.http.request('delete', this.link, {body: {id}}).toPromise();
  }

  async editPerson(person: Person) {
    return this.http.put(this.link, person, this.options).toPromise();
  }
}
